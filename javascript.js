
const user = document.getElementById('user');
const comp = document.getElementById('computer');
const ties = document.getElementById('ties');

const rock = document.getElementById('rock');
const paper = document.getElementById('paper');
const scissors = document.getElementById('scissors');

const messages = document.getElementById('game-messages');
const compMessage = document.getElementById('computer-choice');

let icon;

let userWins = compWins = tiesNum = 0;

let userChoice, compChoice; 
rock.addEventListener('click', function() {
    userChoice = 'rock';
    compChoice = computerPlay();
    computerIcon();
    playRound(userChoice, compChoice);
});

paper.addEventListener('click', function() {
    userChoice = 'paper';
    compChoice = computerPlay();
    computerIcon();
    playRound(userChoice, compChoice);
});

scissors.addEventListener('click', function() {
    userChoice = 'scissors';
    compChoice = computerPlay();
    computerIcon();
    playRound(userChoice, compChoice);
});


function playRound(playerSelection, computerSelection) {
    
    switch (playerSelection) {

        case computerSelection:
            messages.innerText = 'Its a Tie!';
            tiesNum++;
            ties.innerText = 'Ties: ' + tiesNum;
            break;

        case 'rock':
            if (computerSelection === 'scissors') {
                messages.innerText = 'You Win! Rock beats Scissors';
                userWins++;
                user.innerText = 'You: ' + userWins;
            } else if (computerSelection === 'paper') {
                messages.innerText ='You Lose! Paper beats Rock';
                compWins++;
                comp.innerText = 'Computer: ' + compWins;
            }
            break;

        case 'scissors':
            if (computerSelection === 'rock') {
                messages.innerText = 'You Lose! Rock beats Scissors';
                compWins++;
                comp.innerText = 'Computer: ' + compWins;
            } else if (computerSelection === 'paper') {
                messages.innerText = 'You Win! Scissors beats Paper';
                userWins++;
                user.innerText = 'You: ' + userWins;
            }
            break; 

        case 'paper':
            if (computerSelection === 'rock') {
                messages.innerText = 'You Win! Paper beats Rock';
                userWins++;
                user.innerText = 'You: ' + userWins;
            } else if (computerSelection === 'scissors') {
                messages.innerText = 'You lose! Scissors beats Paper';
                compWins++;
                comp.innerText = 'Computer: ' + compWins;
            }
            break;
    }
}

function computerPlay () {
    function getRandomInt(maxNum) {
        return Math.floor(Math.random() * Math.floor(maxNum));
    }

    let computerChoice;
    switch (getRandomInt(3)) {
        case 0:
            computerChoice = 'rock';
            icon = '<i class="far fa-hand-rock"></i>';
            break;
        case 1:
            computerChoice = 'paper';
            icon = '<i class="far fa-hand-paper"></i>';
            break;
        case 2:
            computerChoice = 'scissors';
            icon = '<i class="far fa-hand-scissors"></i>';
            break;
    }    
    return computerChoice;
}
function computerIcon(compPlay) {
    compMessage.innerHTML = 'Computer: ';
    compMessage.innerHTML += icon;
}